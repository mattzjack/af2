from af2 import *

class MyApp(App):
    def appStarted(app):
        app.x = 0 

    def timerFired(app):
        app.x += 1

    def redrawAll(app, canvas):
        canvas.create_rectangle(0, 0, app.x, app.height / 2)

    # def mousePressed(app, event):
    #     app.x = event.x

    def keyPressed(app, event):
        if event.char == ' ':
            app.x = 0

run(MyApp, 500, 500)

